package tsc.abzalov.tm.api.controller;

public interface IProjectTaskController {

    void addTaskToProject() throws Exception;

    void showProjectTasks() throws Exception;

    void deleteProjectTask() throws Exception;

    void deleteProjectWithTasks() throws Exception;

}
