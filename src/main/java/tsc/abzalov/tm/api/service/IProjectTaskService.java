package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    int indexOf(Task task);

    boolean hasData();

    void addTaskToProjectById(String projectId, String taskId) throws Exception;

    Project findProjectById(String id) throws Exception;

    Task findTaskById(String id) throws Exception;

    List<Task> findProjectTasksById(String projectId) throws Exception;

    void deleteProjectById(String id) throws Exception;

    void deleteProjectTasksById(String projectId) throws Exception;

    void deleteProjectTaskById(String taskId) throws Exception;

}
