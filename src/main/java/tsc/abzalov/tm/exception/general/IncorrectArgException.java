package tsc.abzalov.tm.exception.general;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.exception.AbstractException;

public final class IncorrectArgException extends AbstractException {

    public IncorrectArgException(@NotNull String argument) {
        super("Argument \"" + argument + "\" is not available!\n" + "Please, use \"-h\" argument.\n");
    }

}
